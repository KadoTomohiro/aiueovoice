import {Injectable} from '@angular/core';
import {BehaviorSubject, fromEvent, Observable, Subject, timer} from 'rxjs';
import {JpNotesService} from '../jp-notes.service';
import {JpNote} from '../jp-note';

@Injectable()
export class VoiceButtonService {
  private _note: JpNote;
  private _audio: HTMLAudioElement;
  private _loaded: Subject<boolean>;

  constructor(public jqNotes: JpNotesService) {
    this._loaded = new BehaviorSubject(false);
    this._audio = new Audio();
    fromEvent(this._audio, 'canplaythrough')
      .subscribe(() => {
        this._loaded.next(true);
      });
  }

  initNote(noteKey: string): JpNote {
    this._note = this.jqNotes.getJpNote(noteKey);
    if (this._note) {
      this._audio.src = `./assets/voice/_${this._note.text}.wav`;
      this._audio.load();
    }
    return this._note;
  }

  play(): void {
    this._audio.play();
    timer(350).subscribe(() => {
      this._audio.pause();
      this._audio.currentTime = 0;
    });
  }

  loaded(): Observable<boolean> {
    return this._loaded.asObservable();
  }
}
