import {Component, Input, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {JpNote} from '../jp-note';
import {VoiceButtonService} from './voice-button.service';

@Component({
  selector: 'aiueo-voice-button',
  templateUrl: './voice-button.component.html',
  styleUrls: ['./voice-button.component.css'],
  providers: [VoiceButtonService]
})
export class VoiceButtonComponent implements OnInit {

  @Input() note;
  jpNote: JpNote;
  loaded: Observable<boolean>;

  constructor(public vm: VoiceButtonService) {
    this.loaded = this.vm.loaded();
  }

  ngOnInit() {
    this.jpNote = this.vm.initNote(this.note);
  }

  play() {
    this.vm.play();
  }
}
