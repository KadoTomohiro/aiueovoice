import { TestBed } from '@angular/core/testing';

import { VoiceButtonService } from './voice-button.service';

describe('VoiceButtonService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VoiceButtonService = TestBed.get(VoiceButtonService);
    expect(service).toBeTruthy();
  });
});
