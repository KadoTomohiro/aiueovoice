import { Injectable } from '@angular/core';
import {consonant, JpNotes, vowel} from './jp-notes';
import {JpNote} from './jp-note';
import {BehaviorSubject, Observable} from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class JpNotesService {

  private _loaded = new BehaviorSubject<boolean>(false);

  noteMap = new Map<string, JpNote>();
  constructor() {
    JpNotes.forEach((note: JpNote) => {
      this.noteMap.set(note.note, note);

    });
    this._loaded.next(true);
  }

  getJpNote(note: string): JpNote {
    return this.noteMap.get(note);
  }

  getVowels() {
    return vowel;
  }
  getConsonants() {
    return consonant;
  }

  loaded(): Observable<boolean> {
    return this._loaded.asObservable();
  }
}

