import {Component, ElementRef, ViewChild} from '@angular/core';
import {timer} from 'rxjs';
import {JpNotesService} from './jp-notes.service';

@Component({
  selector: 'aiueo-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public vowels: string[];
  public consonants: string[];
  constructor(private jpNoteService: JpNotesService) {
    this.vowels = this.jpNoteService.getVowels();
    this.consonants = this.jpNoteService.getConsonants();
  }
}
