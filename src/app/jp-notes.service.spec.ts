import { TestBed } from '@angular/core/testing';

import { JpNotesService } from './jp-notes.service';

describe('JpNotesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: JpNotesService = TestBed.get(JpNotesService);
    expect(service).toBeTruthy();
  });
});
